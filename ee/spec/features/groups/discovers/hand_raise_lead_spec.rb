# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Groups > Discovers > Hand Raise Lead', :js, :saas, feature_category: :activation do
  include Features::HandRaiseLeadHelpers

  let_it_be(:user) { create(:user, :with_namespace, organization: 'YMCA') }
  let_it_be(:group) do
    create(:group_with_plan, plan: :ultimate_trial_plan, trial_ends_on: Date.tomorrow, owners: user)
  end

  before do
    stub_saas_features(subscriptions_trials: true)
    stub_experiments(trial_discover_page: :candidate)
    # TODO: The below can be removed once trial_discover_page experiment is cleaned up
    # https://gitlab.com/gitlab-org/gitlab/-/issues/439391
    allow_next_instance_of(::Groups::DiscoversController) do |instance|
      allow(instance).to receive(:authorize_discover_page).and_return(true)
    end

    sign_in(user)

    visit group_discover_path(group)
  end

  context 'when user interacts with hand raise lead and submits' do
    it 'renders and submits the top of the page instance' do
      all_by_testid('trial-discover-hand-raise-lead-button').first.click

      fill_in_and_submit_hand_raise_lead(user, group, glm_content: 'trial_discover_page')
    end

    it 'renders and submits the bottom of the page instance' do
      all_by_testid('trial-discover-hand-raise-lead-button').last.click

      fill_in_and_submit_hand_raise_lead(user, group, glm_content: 'trial_discover_page')
    end
  end
end
